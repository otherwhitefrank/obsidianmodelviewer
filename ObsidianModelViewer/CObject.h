/*	Author: Frank Dye
	Date: 9/19/13
	Copyright: Frank Dye, 9/19/2013
	Description: Declaration for CObject structure, all concepts of a landscape, a character, etc.
				Start off with the concept of an object. It should have a object transformation, and a 
				pointer to a number of model structures. 
				For now i'm only concerned with a Object that has a single model attached
				I may rework this for character animation/landscapes at a later date.
				If so they would simply inherit off Object and implement there own tree structure for bones
				number of "groups" of geometry attach to them, etc.

*/

#ifndef	COBJECT_HEADER
#define COBJECT_HEADER

#include "CRoot.h"
#include "CBoundCube.h"
#include "CModel.h"
#include "d3dx11.h"

class CObject : public CRoot
{
public:
    //Constructors
    CObject();
    CObject(CModel* newModel);
    CObject(CModel* newModel, D3DXMATRIX* newTrans);  
    //Destructors
    ~CObject();

    //Visibility checks
    bool    VertexInObjectBounds(float x, float y, float z);
    
    //Transformation functions
    void    SetObjectTransform(D3DXMATRIX* newTrans);
    D3DXMATRIX* GetObjectTransform() {return &objectTransform;};

    //Member functions for attaching models to this object.
    void    AttachModel(CModel* newModel);
    CModel* GetModel() {return pModel;};

private:
    D3DXMATRIX  objectTransform; //D3D8 4x4 Matrix for transformation
    CBoundCube  objectBoundCube; //Entire objects bounding cube, for visibility testing
    CModel*     pModel;          //Pointer to geo
};

#endif