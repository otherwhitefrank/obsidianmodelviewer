/*	Author: Frank Dye
	Date: 9/19/13
	Copyright: Frank Dye, 9/19/2013
	Description: Implementation of Bounding Cube
*/

#include "CBoundCube.h"

CBoundCube::CBoundCube()
{
    min.x = 0;
    min.y = 0;
    min.z = 0;
    max.x = 0;
    max.y = 0;
    max.z = 0;
}

void CBoundCube::AddVertex(float x, float y, float z)
{
    if (x > max.x)
        max.x = x;
    if (x < min.x)
        min.x = x;
    if (y > max.y)
        max.y = y;
    if (y < min.y)
        min.y = y;
    if (z > max.z)
        max.z = z;
    if (z < min.z)
        min.z = z;
}


void CBoundCube::Reset()
{
	min.x = 0.0f;
	min.y = 0.0f;
	min.z = 0.0f;
	max.x = 0.0f;
	max.y = 0.0f;
	max.z = 0.0f;
}

Vector3D* CBoundCube::GetMin()
{
	return &min;
}

Vector3D* CBoundCube::GetMax()
{
    return &max;
}
