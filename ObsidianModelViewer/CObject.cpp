/*	Author: Frank Dye
	Date: 9/19/13
	Copyright: Frank Dye, 9/19/2013
	Description: Implementation of the CObject class
*/

#include "CObject.h"

CObject::CObject()
{

	//Set the transformation to identity
	D3DXMatrixIdentity(&objectTransform);

	//Set the geo pointer to null
	pModel = NULL;
}

CObject::CObject(CModel* newModel)
{
	D3DVector* tempVectorArray;
	int	numVertices;

	//Set transform to identity
	D3DXMatrixIdentity(&objectTransform);

	//Set pointer to new geo
	pModel = newModel;

	//Transform models faces to world space
	pModel->TransformModel();

	//Setup are bounding cube for each face we have.
	numVertices = pModel->GetNumVertices();
	tempVectorArray = pModel->GetWorldSpaceVertices();


	for (int i = 0; i < numVertices; i++)
		objectBoundCube.AddVertex(tempVectorArray[i].posX, tempVectorArray[i].posY, tempVectorArray[i].posZ);

}

CObject::CObject(CModel* newModel, D3DXMATRIX* newTrans)
{
	D3DVector* tempVectorArray = NULL;
	int numVertices;

	//Set the object transformation to the same values as newTrans
	//First we set it to identity then add newTrans onto it
	D3DXMatrixIdentity(&objectTransform);

	objectTransform += *newTrans;


	//Now assign the new model
	pModel= newModel;

	//Set the models transform
	pModel->SetModelTransform(&objectTransform);

	//Transform the models faces to world space
	pModel->TransformModel();

	//Setup are bounding cube for each new face
	numVertices = pModel->GetNumVertices();
	tempVectorArray = pModel->GetWorldSpaceVertices();

	for (int i = 0; i < numVertices; i++)
		objectBoundCube.AddVertex(tempVectorArray[i].posX,tempVectorArray[i].posY, tempVectorArray[i].posZ);
}

CObject::~CObject()
{
	//Make sure the associated geometry is deleted! :)
	delete pModel;
}

bool CObject::VertexInObjectBounds(float x, float y, float z)
{
	Vector3D*		min;
	Vector3D*		max;
	
	min = objectBoundCube.GetMin();
	max = objectBoundCube.GetMax();

	if ((x > min->x && x < max->x) && (y > min->y && y < max->y) && (z > min->z && z < max->z))
		return true;
	else
		return false;
}


void CObject::SetObjectTransform(D3DXMATRIX* newTrans)
{
	//First we set the objects transform to identity, then we add the new one ontop of it
	D3DXMatrixIdentity(&objectTransform);
	objectTransform += *newTrans;
}

void CObject::AttachModel(CModel* newModel)
{
	//UNSAFE, need to be careful with this
	if (pModel != NULL)
		delete pModel;

	pModel = newModel;
}


	

