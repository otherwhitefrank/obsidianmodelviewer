/*	Author: Frank Dye
	Date: 9/19/13
	Copyright: Frank Dye, 9/19/2013
	Description: Implementation of very simple camera
*/

#include "ccamera.h"

void CCamera::ResetCamera()
{
    camRoll = 0;
    camPitch = 0;
    camYaw = 0;

    camPos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
    camTarget = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
    upVect = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
}

CCamera::CCamera(LPDIRECT3DDEVICE9 inDevice)
{
    ResetCamera();
    //Set the pointer to the local instant of the D3D Device
    inDevice->AddRef();
    pD3D9LocalDev = inDevice;
}

CCamera::CCamera(LPDIRECT3DDEVICE9 inDevice, D3DXMATRIX inViewMat, D3DXMATRIX inProjMat)
{
    ResetCamera();
    viewMat = inViewMat;
    inProjMat = inProjMat;
    inDevice->AddRef();
    pD3D9LocalDev = inDevice; 
}

CCamera::CCamera(LPDIRECT3DDEVICE9 inDevice, D3DXVECTOR3 inCamPos, D3DXVECTOR3 inCamTarget, float nearClip, float farClip)
{
    ResetCamera();
    camPos = inCamPos;
    camTarget = inCamTarget;
    nearClipPlane = nearClip;
    farClipPlane = farClip;
    inDevice->AddRef();
    pD3D9LocalDev = inDevice; 
}
 
CCamera::~CCamera()
{
    if (pD3D9LocalDev != NULL)
        pD3D9LocalDev->Release();
}

void CCamera::UpdateView()
{
    //upVect = D3DXVECTOR3(0.0f, 1.0f, 0.0f);

    D3DXMatrixIdentity(&worldMat);
    pD3D9LocalDev->SetTransform(D3DTS_WORLD, &worldMat);

    //Updates the transforms for are viewpoint/projection
	D3DXMatrixLookAtLH(&viewMat, &camPos, &camTarget, &upVect);
    pD3D9LocalDev->SetTransform( D3DTS_VIEW, &viewMat );


    D3DXMatrixPerspectiveFovLH( &projMat, D3DX_PI/4, 1.0f, nearClipPlane, farClipPlane);
    pD3D9LocalDev->SetTransform( D3DTS_PROJECTION, &projMat );
}

D3DXMATRIX  CCamera::GetViewMatrix()
{
    return viewMat;
}


D3DXMATRIX  CCamera::GetProjectionMatrix()
{
    return projMat;
}

float CCamera::GetCameraPitch()
{
    return camPitch; //returns in Radians
}

float CCamera::GetCameraRoll()
{
    return camRoll;
}

float CCamera::GetCameraYaw()
{
    return camYaw;
}

D3DXVECTOR3 CCamera::GetCameraPosition()
{
    return camPos;
}

D3DXVECTOR3 CCamera::GetCameraTarget()
{
    return camTarget;
}

void    CCamera::SetCameraPitch(float inPitch)
{
    camPitch = inPitch;
}

void    CCamera::SetCameraRoll(float inRoll)
{
    camRoll = inRoll;
}

void    CCamera::SetCameraYaw(float inYaw)
{
    camYaw = inYaw;
}

void CCamera::SetCameraViewMatrix(D3DXMATRIX inMat)
{
    viewMat = inMat;
    camPos = D3DXVECTOR3(viewMat._41, viewMat._42, viewMat._43);
}


void CCamera::SetCameraProjectionMatrix(D3DXMATRIX inMat)
{
    projMat = inMat;
}


void    CCamera::SetCameraPosition(D3DXVECTOR3  inPos)
{
    camPos = inPos;
}

void    CCamera::SetCameraTarget(D3DXVECTOR3    inTarget)
{
    camTarget = inTarget;
}

