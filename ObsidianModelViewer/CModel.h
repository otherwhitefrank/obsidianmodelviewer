/*	Author: Frank Dye
	Date: 9/19/13
	Copyright: Frank Dye, 9/19/2013
	Description: CModel class, this class holds the concept of a contigous piece of geo.
				For now It is all elements of a single model. It has a bounding volume specific
				to the geo, it has a geo transform, for now all materials will be logged with a material
				Manager and each face will know a global Scene ID for it's particular material
				Once all models that are to be displayed are known the scene manager will streamline
				all faces according to material and group it's associated model for drawing triangle strips.


				The constructor of the CModel takes in the number of vertices and meshes plus a pointer
				to each.
*/


#ifndef	CMODEL_HEADER
#define CMODEL_HEADER

#include "CRoot.h"
#include "CBoundCube.h"
#include "Elements.h"
#include "D3dx9math.h"

class   CModel : public CRoot
{
public:
    //Constructors
    CModel();
    CModel(D3DVector* newVertices, int inNumVertices, Mesh* inMeshList, int inNumMeshes);
    CModel(D3DVector* newVertices, int inNumVertices, Mesh* inMeshList, int inNumMeshes, D3DXMATRIX* newTrans);

    //Destructors
    ~CModel();

    //Set/Get transform for Geo
    void    SetModelTransform(D3DXMATRIX* newTrans);
    D3DXMATRIX* GetModelTransform() {return &modelTransform;};

    //Attach faces to the geo
    void    AttachVerticesToModel(D3DVector* newVertices, int inNumVertices);
	void	AttachMeshesToModel(Mesh* inMeshList);

    //Functions to get the model and transformed faces
    D3DVector*    GetModelSpaceVertices();
    D3DVector*    GetWorldSpaceVertices();
    Mesh*		  GetMeshList();

    //Functions to transform from model to world space.
    void    TransformModel();
	int		GetNumVertices();
	int		GetNumMeshes();

private:

	int	        numVertices;
	int			numMeshes;
    D3DXMATRIX  modelTransform;
    D3DVector*  pModelSpaceVertices;
    D3DVector*  pWorldSpaceVertices;
	Mesh*		pMeshList;
    CBoundCube  modelBoundCube;
};

#endif