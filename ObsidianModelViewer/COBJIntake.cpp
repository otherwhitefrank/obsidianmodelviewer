/*	Author: Frank Dye
	Date: 9/19/13
	Copyright: Frank Dye, 9/19/2013
	Description: Object to process a .obj file that has been opened and then passed into it 
				via parameter in Constructor or external message call.
*/

#include "COBJIntake.h"

#define	MAX_STRING_SIZE	2056

COBJIntake::COBJIntake()
{
	
	//Count members
	m_VertexCount = 0;
	m_VertexNormalCount = 0;
	m_TextureVertexCount = 0;
	m_NumD3DVertices = 0;
	m_NumMeshes = 0;

}

COBJIntake::COBJIntake(FILE* inputFile)
{
	COBJIntake();

	if(!ferror(inputFile))
	  LoadFile(inputFile);		
}

COBJIntake::~COBJIntake()
{
	ResetObject();
}

void COBJIntake::CountElementsInFile()
{
	char	szTemp[MAX_STRING_SIZE];
    char    token[MAX_STRING_SIZE];
	int		i;

	while (!feof(m_InputFile))
	{
		fgets(szTemp, MAX_STRING_SIZE, m_InputFile);

        //Copy the first word into token
        for ( i = 0; szTemp[i] != '\0' && szTemp[i] != ' '; i++)
            token[i] = szTemp[i];
        token[i] = '\0';

		if (strcmp(token, COMMENT_ID) == 0) 
			//Ignore if a comment
			;
		else if (strcmp(token, VERTEXTEXTURE_ID) == 0)
		{
			m_TextureVertexCount++;
		}
		else if (strcmp(token, VERTEXNORMAL_ID) == 0)
		{
			m_VertexNormalCount++;
		}
		else if (strcmp(token, VERTEX_ID) == 0)
		{			
			m_VertexCount++;
		}
		else if (strcmp(token, FACE_ID) == 0)
		{
			m_NumD3DVertices+=3;
		}
		else if (strcmp(token, GROUPNAME_ID) == 0)
		{
			//Check to see if it is an empty group, this exporter does that!
			for (i = 0; szTemp[i] != '\0' && szTemp[i] != ' '; i++)
				; //Cycle to the end of the first token in the string

			if (szTemp[i] != '\0')
				m_NumMeshes++;
		}

		memset(szTemp, 0, MAX_STRING_SIZE);
	}
	rewind(m_InputFile);
}


void COBJIntake::CopyTextureVertexArray(Vector2D *TextureVertexArray)
{
	memcpy(TextureVertexArray, pTextureVertexArray, sizeof(Vector2D) * m_TextureVertexCount);

}

void COBJIntake::CopyVertexArray(Vector3D *VertexArray)
{
	memcpy(VertexArray, pVertexArray, sizeof(Vector3D) * m_VertexCount);
}

void COBJIntake::CopyVertexNormalArray(Vector3D *VertexNormalArray)
{
	memcpy(VertexNormalArray, pVertexNormalArray, sizeof(Vector3D) * m_VertexNormalCount);
}


void COBJIntake::CopyD3DVectorArray(D3DVector *D3DVectorArray)
{
	memcpy(D3DVectorArray, pD3DVectorArray, sizeof(D3DVector) * m_NumD3DVertices);
}

void COBJIntake::CopyMeshArray(Mesh* inMeshList)
{
	memcpy(inMeshList, pMeshList, sizeof(Mesh) * m_NumMeshes);
}

//Functions to return the pointer

Vector3D*		COBJIntake::GetVertexArray()
{
	return pVertexArray;
}

Vector3D*		COBJIntake::GetVertexNormalArray()
{
	return pVertexNormalArray;
}

Vector2D*		COBJIntake::GetTextureVertexArray()
{
	return pTextureVertexArray;
}



D3DVector* COBJIntake::GetD3DVectorArray()
{
	return pD3DVectorArray;
}

Mesh* COBJIntake::GetMeshArray()
{
	return pMeshList;
}


int COBJIntake::LoadFile(FILE* inputFile)
{
	if (ferror(inputFile))
		return 1;

	m_InputFile = inputFile;
	
	ProcessFile();

	return 0;
}

void COBJIntake::ResetObject()
{
	//Set all parameters to default values
	
	
	//Pointers
	if (m_VertexCount > 0)
		delete [] pVertexArray;
	if (m_VertexNormalCount > 0)
		delete [] pVertexNormalArray;
	if (m_TextureVertexCount > 0)
		delete [] pTextureVertexArray;
	if (m_NumD3DVertices > 0)
		delete [] pD3DVectorArray;

	//Count members
	m_VertexCount = 0;
	m_VertexNormalCount = 0;
	m_TextureVertexCount = 0;
	m_NumD3DVertices = 0;


}

void COBJIntake::ProcessFile()
{
	char	szTemp[MAX_STRING_SIZE];
    char    token[MAX_STRING_SIZE];
	char	typeID[MAX_STRING_SIZE];

	int		vertexIndex = 0;
	int		vertexNormalIndex = 0;
	int		textureVertexIndex = 0;
	int		D3DVectorIndex = 0;
	int		activeMeshIndex = 0;
	bool	readingIntoMesh = false;
	
	//Index variables
	int		i;


	

	//First get the number of elements in the file
	CountElementsInFile();



	//Now create the arrays
	if (m_VertexCount > 0)
		pVertexArray = new Vector3D[m_VertexCount];
	if (m_VertexNormalCount > 0)
		pVertexNormalArray = new Vector3D[m_VertexNormalCount];
	if (m_TextureVertexCount > 0)
		pTextureVertexArray = new Vector2D[m_TextureVertexCount];
	if (m_NumD3DVertices > 0)
		pD3DVectorArray = new D3DVector[m_NumD3DVertices];
	if (m_NumMeshes > 0)
		pMeshList = new Mesh[m_NumMeshes];

	//Clear the D3DVectors out
	for (i = 0; i < m_NumD3DVertices; i++)
	{
		WipeD3DVector(pD3DVectorArray[i]);
	}


	//Start processing the file grabbing a string at a time
	while (!feof(m_InputFile))
	{
		fgets(szTemp, MAX_STRING_SIZE, m_InputFile);


        //Copy the first word into token
        for (i = 0; szTemp[i] != '\0' && szTemp[i] != ' '; i++)
            token[i] = szTemp[i];
        token[i] = '\0';

		if (strcmp(token, COMMENT_ID) == 0) 
			//Ignore if a comment
			;
		else if (strcmp(token, VERTEXTEXTURE_ID) == 0)
		{
			sscanf(szTemp, "%s %f %f", typeID, &pTextureVertexArray[textureVertexIndex].x,
						&pTextureVertexArray[textureVertexIndex].y);

            pTextureVertexArray[textureVertexIndex].y = 1 - pTextureVertexArray[textureVertexIndex].y;

            ++textureVertexIndex;
		}
		else if (strcmp(token, VERTEXNORMAL_ID) == 0)
		{
			sscanf(szTemp, "%s %f %f %f", typeID, &pVertexNormalArray[vertexNormalIndex].x,
						&pVertexNormalArray[vertexNormalIndex].y, &pVertexNormalArray[vertexNormalIndex].z);

			++vertexNormalIndex;

		}
		else if (strcmp(token, VERTEX_ID) == 0)
		{
			sscanf(szTemp, "%s %f %f %f", typeID, &pVertexArray[vertexIndex].x,
			  		&pVertexArray[vertexIndex].y, &pVertexArray[vertexIndex].z);

			++vertexIndex;
		}
		else if (strcmp(token, FACE_ID) == 0)
		{

			ProcessFace(szTemp, &pD3DVectorArray[D3DVectorIndex]);
			
			D3DVectorIndex+=3;
		}
		else if (strcmp(token, GROUPNAME_ID) == 0)
		{
	        for (i = 0; szTemp[i] != '\0' && szTemp[i] != ' '; i++)
				;
			if (szTemp[i] != '\0')
			{

				sscanf(szTemp, "%s %s", typeID, &szTemp);
				if (readingIntoMesh)
				{
					pMeshList[activeMeshIndex].stopIndex = D3DVectorIndex -1;
					activeMeshIndex++;
				}
				else
				{
					readingIntoMesh = true;
				}

				pMeshList[activeMeshIndex].groupName = new char[strlen(szTemp)+1];
				strcpy(pMeshList[activeMeshIndex].groupName, szTemp);

				pMeshList[activeMeshIndex].startIndex = D3DVectorIndex;
			}
		}
		else if (strcmp(token, MATERIAL_ID) == 0)
		{
			sscanf(szTemp, "%s %s", typeID, &szTemp);
			//We got a new material, so we grab it's details from the 
			//material library then hand it to the material manager
			//then store the matManager id in the active mesh.
			pMeshList[activeMeshIndex].materialID = ProcessMaterial(szTemp);
			
		}
		else if (strcmp(token, MATERIALLIB_ID) == 0)
		{
			sscanf(szTemp, "%s %s", typeID, szTemp);
		
			//Add removes duplicates, we just want to add it for now
			//we will go through them later on when we resolve the texture names
			m_MaterialLibraryList.AddString(szTemp);
		}

		memset(szTemp, 0, MAX_STRING_SIZE);

	}

	//make sure a mesh is closed at the end of the file.
	if (readingIntoMesh)
		pMeshList[activeMeshIndex].stopIndex = D3DVectorIndex - 1;
}

int		COBJIntake::ProcessMaterial(char* materialName)
{
	char	szTemp[1024];
	char	szCWD[1024];
	char	szMaterialRef[1024];
	char	szTextureFileName[1024];
	char	token[128];
	bool	foundMaterial = false;
	int		openedFile = 0;
	bool	materialNameMatches = false;

	int		i;

	int		numMaterialLibraries;
	FILE*	fHandle;
	
	float	r, g, b, power;


	//Make a new material and initialize it.
	MaterialDesc* pNewMaterial = new MaterialDesc;

	WipeMaterial(pNewMaterial);


	//Open the materialLibraries 
	numMaterialLibraries = m_MaterialLibraryList.numStrings();

	for ( i = 0; i < numMaterialLibraries; i++)
	{
		m_MaterialLibraryList.SetPTRToStringIndex(szTemp, i);
		_getcwd(szCWD, 1024);
		strcat_s(szCWD, "\\Obj\\");
		strcat_s(szCWD, szTemp);
//		fHandle = fopen(szCWD, "r");
		openedFile = fopen_s(&fHandle, szCWD, "r");

		while (!feof(fHandle))
		{
			fgets(szTemp, MAX_STRING_SIZE, fHandle);

            //Copy the first word into token
            for (i = 0; szTemp[i] != '\0' && szTemp[i] != ' '; i++)
                token[i] = szTemp[i];
            token[i] = '\0';

			if (strcmp(token, MTL_LIBRARY_REF) == 0)
			{
				sscanf(szTemp, "%s %s", &szCWD /* reusing string */
						, &szMaterialRef);
				
				if (strcmp(materialName, szMaterialRef) == 0)
				{
					foundMaterial = true;
					materialNameMatches = true;
					strcpy_s(pNewMaterial->sMaterialRef, materialName);
				}
				else
					materialNameMatches = false;
				
			}

			if (foundMaterial && materialNameMatches)
			{
				if (strcmp(token, MTL_LIBRARY_AMBIENTTEXT) == 0)
				{
					sscanf(szTemp, "%s %s", szMaterialRef, szTextureFileName);
					pNewMaterial->pAmbientTextureFileName = new char[strlen(szTextureFileName)+1];
					memcpy((void*) pNewMaterial->pAmbientTextureFileName, (void*)szTextureFileName, strlen(szTextureFileName) + 1);
				}
				else if (strcmp(token, MTL_LIBRARY_SPECTEXT) == 0)
				{
					sscanf(szTemp, "%s %s", szMaterialRef, szTextureFileName);
					pNewMaterial->pSpecularTextureFileName = new char[strlen(szTextureFileName)+1];
					memcpy((void*) pNewMaterial->pSpecularTextureFileName, (void*)szTextureFileName, strlen(szTextureFileName) + 1);
				}
				else if (strcmp(token, MTL_LIBRARY_DIFFUSETEXT) == 0)
				{
					sscanf(szTemp, "%s %s", szMaterialRef, szTextureFileName);
					pNewMaterial->pDiffuseTextureFileName = new char[strlen(szTextureFileName)+1];
					memcpy((void*) pNewMaterial->pDiffuseTextureFileName, (void*)szTextureFileName, strlen(szTextureFileName) + 1);
				}
				else if (strcmp(token, MTL_LIBRARY_BUMPTEXT) == 0)
				{
					sscanf(szTemp, "%s %s", szMaterialRef, szTextureFileName);
					pNewMaterial->pBumpTextureFileName = new char[strlen(szTextureFileName)+1];
					memcpy((void*) pNewMaterial->pBumpTextureFileName, (void*)szTextureFileName, strlen(szTextureFileName) + 1);
				}
				else if (strcmp(token, MTL_LIBRARY_OPACITYTEXT) == 0)
				{
					sscanf(szTemp, "%s %s", szMaterialRef, szTextureFileName);
					pNewMaterial->pOpacityTextureFileName = new char[strlen(szTextureFileName)+1];
					memcpy((void*) pNewMaterial->pOpacityTextureFileName, (void*)szTextureFileName, strlen(szTextureFileName) + 1);
				}

				else if (strcmp(token, MTL_LIBRARY_AMBIENT) == 0)
				{
					sscanf(szTemp, "%s %f %f %f", &szMaterialRef /* reusing string */, &r, &g, &b);
					pNewMaterial->mat.Ambient.r = r;
					pNewMaterial->mat.Ambient.g = g;
					pNewMaterial->mat.Ambient.b = b;
					pNewMaterial->mat.Ambient.a = 0.0f;   //All alpha's are 0 for now
				}
				else if (strcmp(token, MTL_LIBRARY_DIFFUSE) == 0)
				{
					sscanf(szTemp, "%s %f %f %f", &szMaterialRef /* reusing string */, &r, &g, &b);
					pNewMaterial->mat.Diffuse.r = r;
					pNewMaterial->mat.Diffuse.g = g;
					pNewMaterial->mat.Diffuse.b = b;
					pNewMaterial->mat.Diffuse.a = 0.0f;
				}
				else if (strcmp(token, MTL_LIBRARY_SPECULAR) == 0)
				{
					sscanf(szTemp, "%s %f %f %f", &szMaterialRef /* reusing string */, &r, &g, &b);
					pNewMaterial->mat.Specular.r = r;
					pNewMaterial->mat.Specular.g = g;
					pNewMaterial->mat.Specular.b = b;
					pNewMaterial->mat.Specular.a = 0.0f;
				}
				else if (strcmp(token, MTL_LIBRARY_SHININESS) == 0)
				{
					sscanf(szTemp, "%s %f", &szMaterialRef, &power);
					pNewMaterial->mat.Power = power;
				}
			}
		}
		fclose(fHandle);
	}


	//Now we have a material, hand it to the materialManager and get a id back
	if (foundMaterial)
		return pMaterialManager->AddMaterial(pNewMaterial);
	else
		return -1;	
}


void	COBJIntake::ProcessFace(char* inputString, D3DVector* D3DInVectors)
{
	bool    readingANumber = false;
	int	    stringSearchIndex = 0;
	int	    stringStartIndex = 0;
	int	    textVertexIndex = 0;
	int     normVertexIndex = 0;
	int     VertexIndex = 0;
	int	    typeID = 0;
	int	    numSlashes = 0;
	char	tokenString[MAX_STRING_SIZE];

	//Reset max vertice count
	int	numVertices = 0;

	//Count number of vertices in this face
	while (inputString[stringSearchIndex] != '\0')
	{
		if (readingANumber)
		{
			if (inputString[stringSearchIndex] == ' ' ||
					inputString[stringSearchIndex] == '\n' ||
					inputString[stringSearchIndex] == '\0')
			{
				numVertices++;
			}
		}
		else
		{
			if (inputString[stringSearchIndex] >= 48 && inputString[stringSearchIndex] < 58)
				readingANumber = true;
		}
		stringSearchIndex++;
	}

	//Now go through the string again and assign the values to the vertices
	
	stringSearchIndex = 0;
	readingANumber = false;
	numVertices = 0;
	
	while (inputString[stringSearchIndex] != '\0')
	{
		//Scan through the 
		if (readingANumber)
		{
			if (inputString[stringSearchIndex] == ' '||
				  inputString[stringSearchIndex] == '\n')
			{
				memset(tokenString, '\0', sizeof(tokenString));
				memcpy(&tokenString, &inputString[stringStartIndex], sizeof(char) * stringSearchIndex - stringStartIndex);
				numSlashes = countNumSlashes(tokenString);

				if (numSlashes == 0)
				{
					sscanf(tokenString, "%i", &VertexIndex);
					D3DInVectors[numVertices].posX = pVertexArray[VertexIndex-1].x;
					D3DInVectors[numVertices].posY = pVertexArray[VertexIndex-1].y;
					D3DInVectors[numVertices].posZ = pVertexArray[VertexIndex-1].z;
				}
				else if (sscanf(tokenString, "%i/%i/%i", &VertexIndex, &textVertexIndex, &normVertexIndex)==3)
				{
					D3DInVectors[numVertices].posX = pVertexArray[VertexIndex-1].x;
					D3DInVectors[numVertices].posY = pVertexArray[VertexIndex-1].y;
					D3DInVectors[numVertices].posZ = pVertexArray[VertexIndex-1].z;
					D3DInVectors[numVertices].normX = pVertexNormalArray[normVertexIndex-1].x;
					D3DInVectors[numVertices].normY = pVertexNormalArray[normVertexIndex-1].y;
					D3DInVectors[numVertices].normZ = pVertexNormalArray[normVertexIndex-1].z;
					D3DInVectors[numVertices].textU = pTextureVertexArray[textVertexIndex-1].x;
					D3DInVectors[numVertices].textV = pTextureVertexArray[textVertexIndex-1].y;

				}
				else if (sscanf(tokenString, "%i//%i", &VertexIndex, &normVertexIndex) == 2)
				{
					D3DInVectors[numVertices].posX = pVertexArray[VertexIndex-1].x;
					D3DInVectors[numVertices].posY = pVertexArray[VertexIndex-1].y;
					D3DInVectors[numVertices].posZ = pVertexArray[VertexIndex-1].z;
					D3DInVectors[numVertices].normX = pVertexNormalArray[normVertexIndex-1].x;
					D3DInVectors[numVertices].normY = pVertexNormalArray[normVertexIndex-1].y;
					D3DInVectors[numVertices].normZ = pVertexNormalArray[normVertexIndex-1].z;

				}
				//Do this last to make sure we dont confuse it with %i/%i/%i
				//no trailing slash
				else if (sscanf(tokenString, "%i/%i", &VertexIndex, &textVertexIndex) == 2)
				{
					D3DInVectors[numVertices].posX = pVertexArray[VertexIndex-1].x;
					D3DInVectors[numVertices].posY = pVertexArray[VertexIndex-1].y;
					D3DInVectors[numVertices].posZ = pVertexArray[VertexIndex-1].z;
					D3DInVectors[numVertices].textU = pTextureVertexArray[textVertexIndex-1].x;
					D3DInVectors[numVertices].textV = pTextureVertexArray[textVertexIndex-1].y;

				}
				readingANumber = false;
				numVertices++;
			}
		}
		else
		{
			if (inputString[stringSearchIndex] >= 48 && inputString[stringSearchIndex] < 58)
			{
				readingANumber = true;
				stringStartIndex = stringSearchIndex;
			}
		}
		stringSearchIndex++;
	}

}

int	COBJIntake::countNumSlashes(char* inputString)
{
	int	numSlashes = 0;
	for (int i = 0; i < int(strlen(inputString)); i++)
	{
		if (inputString[i] == '/')
			numSlashes++;
	}
	return numSlashes;
}

void COBJIntake::WipeMaterial(MaterialDesc* inMat)
{
	//Erase a material that is passed in
	inMat->mat.Ambient.r = 0.0f;
    inMat->mat.Ambient.g = 0.0f;
    inMat->mat.Ambient.b = 0.0f;
    inMat->mat.Ambient.a = 0.0f;

    inMat->mat.Specular.r = 0.0f;
    inMat->mat.Specular.g = 0.0f;
    inMat->mat.Specular.b = 0.0f;
    inMat->mat.Specular.a = 0.0f;

    inMat->mat.Diffuse.r = 0.0f;
    inMat->mat.Diffuse.g = 0.0f;
    inMat->mat.Diffuse.b = 0.0f;
    inMat->mat.Diffuse.a = 0.0f;

    inMat->mat.Emissive.r = 0.0f;
    inMat->mat.Emissive.g = 0.0f;
    inMat->mat.Emissive.b = 0.0f;
    inMat->mat.Emissive.a = 0.0f;

    inMat->mat.Power = 0.0f;

	//Set are name arrays all to null
    inMat->pAmbientTextureFileName = NULL;
    inMat->pDiffuseTextureFileName = NULL;
    inMat->pSpecularTextureFileName = NULL;
    inMat->pOpacityTextureFileName = NULL;
    inMat->pBumpTextureFileName = NULL;

}

void	COBJIntake::WipeD3DVector(D3DVector& inVect)
{
	//Wipe a D3DVector;
	inVect.posX = 0.0f;
	inVect.posY = 0.0f;
	inVect.posZ = 0.0f;
	inVect.normX = 0.0f;
	inVect.normY = 0.0f;
	inVect.normZ = 0.0f;
	inVect.textU = 0.0f;
	inVect.textV = 0.0f;
}