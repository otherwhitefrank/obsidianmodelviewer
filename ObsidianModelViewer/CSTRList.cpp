/*	Author: Frank Dye
	Date: 9/19/13
	Copyright: Frank Dye, 9/19/2013
	Description: Implementation of CSTRList

*/

#include "CSTRList.h"
#include <tchar.h>

CSTRList::CSTRList()
{
	head = NULL;
}

CSTRList::~CSTRList()
{
	ResetList();
}

bool	CSTRList::AddString(char* aString)
{
	bool	stringFound = false;
	
	//Search for a duplicate entry
	tempNode = head;
	if (head == NULL)
	{
		//Initialize the list
		head = new STRListNode;
		head->nextNode = NULL;
		memcpy(head->szSTR, aString, sizeof(char) * strlen(aString) +1);
	}
	else
	{
		do 
		{
			if (strcmp(aString, tempNode->szSTR) == 0)
				stringFound = true;
			if (tempNode->nextNode != NULL)
				tempNode = tempNode->nextNode;
		} while (tempNode->nextNode != NULL && !stringFound);

		if (stringFound == false)
		{
			tempNode->nextNode = new STRListNode;
			tempNode = tempNode->nextNode;
			tempNode->nextNode = NULL;
			memcpy(tempNode->szSTR, aString, sizeof(char) * strlen(aString) + 1);
		}
	}

	return !stringFound;
}

bool	CSTRList::DelString(char*	aString)
{
	bool	stringFound = false;
	STRListNode*	oneNodeAhead;
	//Search for the entry
	tempNode = head;
	oneNodeAhead = head;
	if (head != NULL)
	{
		if (strcmp(aString, tempNode->szSTR) == 0)
			stringFound = true;

		while (tempNode->nextNode != NULL && !stringFound)
		{
			if (tempNode->nextNode != NULL)
			{
				oneNodeAhead = tempNode;
				tempNode = tempNode->nextNode;
			}
			if (strcmp(aString, tempNode->szSTR) == 0)
			{
				stringFound = true;
			}

		} 

		if (stringFound == true)
		{	
			if (tempNode->nextNode != NULL)
				oneNodeAhead->nextNode = tempNode->nextNode;
			else	
				oneNodeAhead->nextNode = NULL;
			
			if (tempNode == head && tempNode->nextNode != NULL)
				head = tempNode->nextNode;

			delete tempNode;
		}
	}

	return stringFound;
}

int		CSTRList::numStrings()
{
	int	numStrings = 1;

	tempNode = head;
	while (tempNode->nextNode != NULL)
	{
		numStrings++;
		tempNode = tempNode->nextNode;
	}
	return numStrings;
}

void	CSTRList::SetPTRToStringIndex(char* szPtr, int _index)
{
	//Copies all entries in the list to the memory locations, assumes it was initialized
	//with enough room for the copy.

	tempNode = head;
	for(int i = 0; i < _index ; i++)
	{
		tempNode = tempNode->nextNode;
	}

	if (tempNode != NULL)
		memcpy((void*)szPtr, (void*)tempNode->szSTR, sizeof(char) * strlen(tempNode->szSTR) + 1);
}

void	CSTRList::ResetList()
{
	STRListNode*	aNode;
	tempNode = head;
	while (tempNode != NULL)
	{
		aNode = tempNode->nextNode;
		delete tempNode;
		tempNode = aNode;
	}

	head = NULL;
}

bool	CSTRList::StringInList(char* aString)
{
	bool	stringFound = false;
	
	//Search for a duplicate entry
	tempNode = head;
	while (tempNode->nextNode != NULL && !stringFound)
	{
		if (strcmp(aString, tempNode->szSTR) == 0)
			stringFound = true;
		if (tempNode->nextNode != NULL)
			tempNode = tempNode->nextNode;
	}
	return stringFound;
}


int		CSTRList::IndexOfString(char* aString)
{
	int		index = 0;
	bool	stringFound = false;
	
	tempNode = head;

	//Check the head first
	if (strcmp(aString, tempNode->szSTR) == 0)
		stringFound = true;
	else
		index++;
	if (!stringFound)
	{
		do 
		{
			if (tempNode->nextNode != NULL)
				tempNode = tempNode->nextNode;

			if (strcmp(aString, tempNode->szSTR) == 0)
				stringFound = true;
			else
				index++;	
		} while (tempNode->nextNode != NULL && !stringFound);
	}
	if (stringFound)
		return index;
	else 
		return -1;
}
	





	
