/*	Author: Frank Dye
	Date: 9/19/13
	Copyright: Frank Dye, 9/19/2013
	Description: Implementation of Material Manager
*/

#include "CMaterialManager.h"

//Implementation of basic hash function
//Basic hash function
int hashFunc(const char* s)   
{
  int key, pos;

  key = 0;
  pos = 0;
  while (*s)
  {
	key += pos * (*s -33);
	s++;
	pos++;
  }
  return( key /* some number should be unique*/ );
}


//Begin CMaterialManager
CMaterialManager::CMaterialManager()
{
	MaterialDescMap.clear();
}

CMaterialManager::~CMaterialManager()
{
	;
}

void	CMaterialManager::SetD3DDevice(LPDIRECT3DDEVICE9	inDevice)
{
	pD3D9Dev = inDevice;
}

int CMaterialManager::AddMaterial(MaterialDesc* inMat)
{
	int tempKey;
	tempKey = hashFunc(inMat->sMaterialRef);

	if (!IsMaterialPresent(inMat))	//Material Is Not Present
	{
		MaterialDescMap.insert(std::map<int, const MaterialDesc*>::value_type(tempKey, inMat));

//		MaterialDescMap[tempKey] = inMat;
		GrabTextures(inMat); //Since we know it is the first time we've had
							 //this material lets open it! :)
	}

	return tempKey;
}

const MaterialDesc* CMaterialManager::GetMaterialHandle(int matHandle)
{
	std::map<int, const MaterialDesc*>::iterator theIterator = MaterialDescMap.find(matHandle);

	if (theIterator != MaterialDescMap.end())
	{
		return (*theIterator).second;
	}
	else
	{
		return NULL;
	}
        
}

int CMaterialManager::GetMaterialID(MaterialDesc* inMat)
{
	return hashFunc(inMat->sMaterialRef);
}

bool CMaterialManager::IsMaterialPresent(MaterialDesc* inMat)
{
	int key;
	bool returnVal;
	std::map<int, const MaterialDesc*>::iterator a;


	key = hashFunc(inMat->sMaterialRef);


	a = MaterialDescMap.find(key);

	if (a != MaterialDescMap.end())
		returnVal = true;
	else
		returnVal = false;

	return returnVal;
}

void CMaterialManager::Reset()
{
	MaterialDescMap.clear(); //Reset the map
}


void CMaterialManager::GrabTextures(MaterialDesc* inMat)
{
	//For each texture are desc has open a texture
	char	szTempFileName[1024];

	//Check for ambient
    if (inMat->pAmbientTextureFileName != NULL)
    {
		_getcwd(szTempFileName, 1024);
	    strcat_s(szTempFileName, "\\Obj\\");
    	strcat_s(szTempFileName, inMat->pAmbientTextureFileName);
		D3DXCreateTextureFromFile( pD3D9Dev, szTempFileName, &inMat->pAmbientTexture);
    }

    //Check for diffuse
    if (inMat->pDiffuseTextureFileName != NULL)
    {
        _getcwd(szTempFileName, 1024);
	    strcat_s(szTempFileName, "\\Obj\\");
    	strcat_s(szTempFileName, inMat->pDiffuseTextureFileName);
		D3DXCreateTextureFromFile( pD3D9Dev, szTempFileName, &inMat->pDiffuseTexture);
    }

    //Check for specular
    if (inMat->pSpecularTextureFileName != NULL)
    {
        _getcwd(szTempFileName, 1024);
	    strcat_s(szTempFileName, "\\Obj\\");
    	strcat_s(szTempFileName, inMat->pSpecularTextureFileName);
		D3DXCreateTextureFromFile( pD3D9Dev, szTempFileName, &inMat->pSpecularTexture);
    }

    //Check for opacity
    if (inMat->pOpacityTextureFileName != NULL)
    {
        _getcwd(szTempFileName, 1024);
	    strcat_s(szTempFileName, "\\Obj\\");
    	strcat_s(szTempFileName, inMat->pOpacityTextureFileName);
		D3DXCreateTextureFromFile( pD3D9Dev, szTempFileName, &inMat->pOpacityTexture);
    }

    //Check for bumpmap
    if (inMat->pBumpTextureFileName != NULL)
    {
        _getcwd(szTempFileName, 1024);
	    strcat_s(szTempFileName, "\\Obj\\");
    	strcat_s(szTempFileName, inMat->pBumpTextureFileName);
		D3DXCreateTextureFromFile( pD3D9Dev, szTempFileName, &inMat->pBumpTexture);
    }
}