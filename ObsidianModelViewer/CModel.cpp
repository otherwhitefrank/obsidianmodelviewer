/*	Author: Frank Dye
	Date: 9/19/13
	Copyright: Frank Dye, 9/19/2013
	Description: Implementation of the CModel class
*/

#include "CModel.h"

CModel::CModel()
{
	numVertices = 0;
	numMeshes = 0;
	
	//Set are transformation to identity
	D3DXMatrixIdentity(&modelTransform);

	//set are face pointers to NULL
	pModelSpaceVertices = NULL;
	pWorldSpaceVertices = NULL;

	modelBoundCube.Reset();

}


CModel::CModel(D3DVector* newVertices, int inNumVertices, Mesh* inMeshList, int inNumMeshes)
{
    numVertices = inNumVertices;
	numMeshes = inNumMeshes;

	int i;

	//Set are transformation to identity
	D3DXMatrixIdentity(&modelTransform);

	//Initialize are face arrays
	pModelSpaceVertices = newVertices;

	//since we have no transform yet just set all the new vertices to there model
	//space values
	pWorldSpaceVertices = new D3DVector[numVertices];

	for ( i = 0; i < numVertices; i++)
	{

		pWorldSpaceVertices[i].posX = pModelSpaceVertices[i].posX;
		pWorldSpaceVertices[i].posY = pModelSpaceVertices[i].posY;
		pWorldSpaceVertices[i].posZ = pModelSpaceVertices[i].posZ;
		pWorldSpaceVertices[i].normX = pModelSpaceVertices[i].normX;
		pWorldSpaceVertices[i].normY = pModelSpaceVertices[i].normY;
		pWorldSpaceVertices[i].normZ = pModelSpaceVertices[i].normZ;
		pWorldSpaceVertices[i].textU = pModelSpaceVertices[i].textU;
		pWorldSpaceVertices[i].textV = pModelSpaceVertices[i].textV;
	
	}
	//Transform the model

	TransformModel();

	//Update the internal bounding cube
	for ( i = 0; i < numVertices; i++)
		modelBoundCube.AddVertex(pWorldSpaceVertices[i].posX, pWorldSpaceVertices[i].posY, pWorldSpaceVertices[i].posZ);

	//Assign the meshlist that was created outside the class.
	pMeshList = inMeshList;

}


CModel::CModel(D3DVector* newVertices, int inNumVertices, Mesh* inMeshList, int inNumMeshes, D3DXMATRIX* newTrans)
{
	numVertices = inNumVertices;
	numMeshes = inNumMeshes;
	int i;

	//Set are transformation to identity then add in are new transformation
	D3DXMatrixIdentity(&modelTransform);

	modelTransform += *newTrans;

	//Initialize are face arrays
	pModelSpaceVertices = newVertices;

	//since we have a transform we set all of the worldspace faces to model space
	//Then transform them

	pWorldSpaceVertices = new D3DVector[numVertices];

	for ( i = 0; i < numVertices; i++)
	{
		pWorldSpaceVertices[i].posX = pModelSpaceVertices[i].posX;
		pWorldSpaceVertices[i].posY = pModelSpaceVertices[i].posY;
		pWorldSpaceVertices[i].posZ = pModelSpaceVertices[i].posZ;
		pWorldSpaceVertices[i].normX = pModelSpaceVertices[i].normX;
		pWorldSpaceVertices[i].normY = pModelSpaceVertices[i].normY;
		pWorldSpaceVertices[i].normZ = pModelSpaceVertices[i].normZ;
		pWorldSpaceVertices[i].textU = pModelSpaceVertices[i].textU;
		pWorldSpaceVertices[i].textV = pModelSpaceVertices[i].textV;
	
	}

	//Transform the model
	TransformModel();

	//Update the internal bounding cube
	for ( i = 0; i < numVertices; i++)
		modelBoundCube.AddVertex(pWorldSpaceVertices[i].posX, pWorldSpaceVertices[i].posY, pWorldSpaceVertices[i].posZ);

	//Assign the meshlist that was created outside the class.
	pMeshList = inMeshList;

}


CModel::~CModel()
{
	//Delete the faces associated with this model

	delete [] pWorldSpaceVertices;
	delete [] pModelSpaceVertices;
}

void CModel::SetModelTransform(D3DXMATRIX* newTrans)
{
	D3DXMatrixIdentity(&modelTransform);
	modelTransform += *newTrans;
}

void CModel::AttachVerticesToModel(D3DVector* newVertices, int inNumVertices)
{

	pModelSpaceVertices = newVertices;

	numVertices = inNumVertices;

	if (pWorldSpaceVertices == NULL)
		pWorldSpaceVertices = new D3DVector[numVertices];

	for (int i = 0; i < numVertices; i++)
	{
		pWorldSpaceVertices[i].posX = pModelSpaceVertices[i].posX;
		pWorldSpaceVertices[i].posY = pModelSpaceVertices[i].posY;
		pWorldSpaceVertices[i].posZ = pModelSpaceVertices[i].posZ;
		pWorldSpaceVertices[i].normX = pModelSpaceVertices[i].normX;
		pWorldSpaceVertices[i].normY = pModelSpaceVertices[i].normY;
		pWorldSpaceVertices[i].normZ = pModelSpaceVertices[i].normZ;
		pWorldSpaceVertices[i].textU = pModelSpaceVertices[i].textU;
		pWorldSpaceVertices[i].textV = pModelSpaceVertices[i].textV;
	
	}

	TransformModel();
}

D3DVector* CModel::GetModelSpaceVertices()
{

	return pModelSpaceVertices;
}

D3DVector* CModel::GetWorldSpaceVertices()
{    
	return pWorldSpaceVertices;
}

Mesh*	   CModel::GetMeshList()
{
	return pMeshList;
}

int	 CModel::GetNumVertices()
{
	return numVertices;
}

int  CModel::GetNumMeshes()
{
	return numMeshes;
}

void CModel::AttachMeshesToModel(Mesh* inMeshList)
{
	pMeshList = inMeshList;
}

void CModel::TransformModel()
{	//Implementation of transformation function.
	//For now uses temporary swap vectors so I can transform using DX8 internal
	//functions. I will soon figure out a way to do this with my own vector/matrix class.

	D3DXVECTOR3 modelVector, worldVector;

	for (int i = 0; i < numVertices; i++)
	{
		modelVector.x = pModelSpaceVertices[i].posX;
		modelVector.y = pModelSpaceVertices[i].posY;
		modelVector.z = pModelSpaceVertices[i].posZ;
			
		D3DXVec3TransformCoord(&worldVector, 
							   &modelVector, 
							   &modelTransform);

		pWorldSpaceVertices[i].posX = worldVector.x;
		pWorldSpaceVertices[i].posY = worldVector.y;
		pWorldSpaceVertices[i].posZ = worldVector.z;

	}
}


