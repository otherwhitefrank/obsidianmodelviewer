/*	Author: Frank Dye
	Date: 9/19/13
	Copyright: Frank Dye, 9/19/2013
	Description: Base member in are hierchy, all components of geometry tree go off this.
*/

#ifndef	CROOT_HEADER
#define CROOT_HEADER

class   CRoot
{
public:
    int GetTypeID() {return typeID;};   //Request the type id, will probably use this in polymorphic
                                        //casting...
private:
    int typeID; //Type ID to identify a higher level type in are structure.
};

#endif