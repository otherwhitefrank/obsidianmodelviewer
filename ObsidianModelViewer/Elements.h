/*	Author: Frank Dye
	Date: 9/19/13
	Copyright: Frank Dye, 9/19/2013
	Description: Basic components for Vertex data and Face information
*/

#ifndef MYELEMENTS_HEADER
#define MYELEMENTS_HEADER


#include <d3d11.h>
#include <d3dx11.h>

//Maximum size for a material name
#define		MAX_MATERIALNAME_SIZE	256

//Maximum number of groups
#define		MAX_GROUP_SIZE	1024

struct	D3DVector
{
	float	posX;
	float	posY;
	float	posZ;
	float   normX;
	float	normY;
	float	normZ;
	float	textU;
	float	textV;

};

struct	Vector3D
{
	float	x;
	float	y;
	float	z;
};

struct	Vector2D
{
	float	x;
	float	y;
};


struct	MaterialDesc
{
	char		    sMaterialRef[MAX_MATERIALNAME_SIZE];    //Original ID for the scene
	char*		    pAmbientTextureFileName;
    char*           pDiffuseTextureFileName;
    char*           pSpecularTextureFileName;
    char*           pOpacityTextureFileName;
    char*           pBumpTextureFileName;

    //Pointers to textures we will initialize! :)
    LPDIRECT3DTEXTURE9  pAmbientTexture;
    LPDIRECT3DTEXTURE9  pDiffuseTexture;
    LPDIRECT3DTEXTURE9  pSpecularTexture;
    LPDIRECT3DTEXTURE9  pOpacityTexture;
    LPDIRECT3DTEXTURE9  pBumpTexture;

    //Are material components
    D3DMATERIAL9    mat;
};

struct	MaterialLibraryDesc
{
	char		sMaterialLibraryName[MAX_MATERIALNAME_SIZE];
};

struct	GroupName
{
	char		sGroup[MAX_GROUP_SIZE];
};

struct  RenderTextureRange
{
	int	min;
	int	max;
};

struct	Mesh
{
	int	startIndex;
	int stopIndex;
	int materialID;
	char* groupName;
};


#endif

