/*	Author: Frank Dye
	Date: 9/19/13
	Copyright: Frank Dye, 9/19/2013
	Description: A header file for a bounding cube class
*/

#ifndef	CBOUNDCUBE_HEADER
#define CBOUNDCUBE_HEADER

#include "Elements.h"

class   CBoundCube
{
public:
    //Constructor
    CBoundCube();
    
    void        AddVertex(float x, float y, float z); //Updates the bounding cube's max/min for this vertice
	void		Reset(); //Resets min/max
	Vector3D*   GetMin();
    Vector3D*	GetMax();

private:
    Vector3D    min;
    Vector3D    max;
};

#endif