/*	Author: Frank Dye
	Date: 9/19/13
	Copyright: Frank Dye, 9/19/2013
	Description: Class to handle the management of each material. When a new model is imported
				it passes it's material information to the material Manager. The material manager
				returns a integer ID that is assigned to each mesh using this material.
				During rendering all meshes are collected and organized, as a mesh is about to be 
				rendered it queries the material manager for a handle to that material and the
				rendering pipeline is configured appropriately.
*/

#ifndef	CMATERIALMANAGER_HEADER
#define CMATERIALMANAGER_HEADER

#include "elements.h"
#include <map>

//Basic hash function
int hashFunc(const char* s);   

class CMaterialManager
{
public:
	CMaterialManager();  //Constructor
	~CMaterialManager(); //Destructor

	int AddMaterial(MaterialDesc* inMat); //Receives the address to a new material
										  //passes back a ID for this material.
	void Reset();	//Resets the manager

	const MaterialDesc*	GetMaterialHandle(int matHandle); //Receive a pointer to the mat handle
	int	GetMaterialID(MaterialDesc* inMat); //Get a ID for a already loaded material

	void	SetD3DDevice(LPDIRECT3DDEVICE9	inDevice);

private:
	bool IsMaterialPresent(MaterialDesc* inMat); //Find out if manager contains this material
	void GrabTextures(MaterialDesc* inMat);	//Allocates all the textures if they are present

//Data
	std::map<int, const MaterialDesc*> MaterialDescMap;
	LPDIRECT3DDEVICE9	pD3D9Dev; //pointer to current device
};

#endif