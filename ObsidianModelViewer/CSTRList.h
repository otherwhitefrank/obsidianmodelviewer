/*	Author: Frank Dye
	Date: 9/19/13
	Copyright: Frank Dye, 9/19/2013
	Description: Class definition for a Dynamicly linked list that stores strings
				 the list has no duplicate entries.
*/

#ifndef	CSTRLIST_HEADER
#define CSTRLIST_HEADER

#define	CSTRLIST_MAX_STRING_SIZE	256

struct	STRListNode;

struct	STRListNode
{
	char	szSTR[CSTRLIST_MAX_STRING_SIZE];
	STRListNode* nextNode;
};

class	CSTRList
{
	public:
	//Destructor/constructor
		CSTRList();
		~CSTRList();

	//Functions 
	bool	AddString(char* aString);
	bool	DelString(char*	aString);
	bool	StringInList(char* aString);
	int		IndexOfString(char* aString);
	
	int		numStrings();
	void	SetPTRToStringIndex(char* szPtr, int _index); //copies the strings to the specified locations in memory
														  //Assumes the PTR matches the num of strings in the list.
	void	ResetList();

	private:
	STRListNode*	head;		//Pointer to the first link in the list

	STRListNode*	tempNode;	//Temp pointer for internal use
};


#endif