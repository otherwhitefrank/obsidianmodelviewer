/*	Author: Frank Dye
	Date: 9/19/13
	Copyright: Frank Dye, 9/19/2013
	Description: Class that receives a file handle to a .obj file, it then processes the .obj
				file and stores all data internally from that file. It can then be queried externally
				and it will pass this information back via public functions.
*/

#ifndef	COBJINTAKE_HEADER
#define COBJINTAKE_HEADER

#include <stdio.h>
#include "Elements.h"
#include "OBJDefs.h"
#include "CSTRList.h"
#include "CMaterialManager.h"

class	COBJIntake
{
public:
	//Constructors
	COBJIntake();
	COBJIntake(FILE* inputFile);
	
	//Deconstructors
	~COBJIntake();

	int     LoadFile(FILE* inputFile); //Load a new file 
	
	//Functions to request counts from file
	int 	GetVertexCount()			{return m_VertexCount;};
	int 	GetTextureVertexCount()	    {return m_TextureVertexCount;};
	int 	GetVertexNormalCount()	    {return m_VertexNormalCount;};
	int 	GetNumD3DVertices()			{return m_NumD3DVertices;};
	int		GetNumMeshes()				{return m_NumMeshes;};

	//Functions to request pointers
	Vector3D*				GetVertexArray(); 
	Vector3D*				GetVertexNormalArray();
	Vector2D*				GetTextureVertexArray();
	D3DVector*				GetD3DVectorArray();
	Mesh*					GetMeshArray();

	//Functions to physically copy out elements.
	void	CopyD3DVectorArray(D3DVector* D3DVectorArray);
	void	CopyMeshArray(Mesh* inMeshList);

	void	CopyVertexArray(Vector3D* VertexArray);  //All perform physical memcpy
	void	CopyVertexNormalArray(Vector3D* VertexNormalArray);
	void	CopyTextureVertexArray(Vector2D* TextureVertexArray);

	
	//Set the materialManager
	void	SetMaterialManager(CMaterialManager* matMan) {pMaterialManager = matMan;};

private:
	void	CountElementsInFile(); //Assumes m_inputFile is set, counts number of vertices
	void	ProcessFile();		   //Assumes m_inputFile is set, fills internal arrays
	void	ResetObject();		   //Resets all member functions to default values

	void	ProcessFace(char* inputString, D3DVector* D3DInVectors);

	int		ProcessMaterial(char* materialName);

	int		countNumSlashes(char* inputString);
	void	ProcessMaterials();

	void	WipeMaterial(MaterialDesc* inMat);
	void	WipeD3DVector(D3DVector& inVect);

	//Internal counts of stores information
	int		m_VertexCount;			//Number of vertices in file
	int		m_TextureVertexCount;	//Number of texture vertices in file
	int		m_VertexNormalCount;	//Number of vertex normals in file

	int		m_NumD3DVertices;		//Number of faces in file
	int		m_NumMeshes;			//Number of meshes in file

	//Pointers to element arrays
	Vector3D*					pVertexArray;			//Pointer to array of vertices
	Vector3D*					pVertexNormalArray;		//Pointer to array of vertex normals
	Vector2D*					pTextureVertexArray;	//Pointer to array of texture vertices
	D3DVector*					pD3DVectorArray;		//Pointer to are Direct3D Faces
	Mesh*						pMeshList;
	CMaterialManager*			pMaterialManager;

	
	//String lists to process materials
	CSTRList m_MaterialLibraryList;

	//File handle
	FILE*	m_InputFile;		//Save a copy of the input file for internal use
	
};

#endif