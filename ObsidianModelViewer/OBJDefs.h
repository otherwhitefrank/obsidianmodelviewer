/*	Author: Frank Dye
	Date: 9/19/13
	Copyright: Frank Dye, 9/19/2013
	Description: Header file containing definitions for OBJ file format 2.11? ish
*/

#ifndef	OBJDEFS_HEADER
#define	OBJDEFS_HEADER

//Misc
#define COMMENT_ID				"#"

//Vertex data
#define	VERTEX_ID				"v"
#define	VERTEXTEXTURE_ID		"vt"
#define	VERTEXNORMAL_ID			"vn"
#define	VERTEXPARAM_ID			"vp"
#define FREEFORM_SURF_ID		"cstype"
#define	DEGREE_ID				"deg"
#define	BASISMATRIX_ID			"bmat"
#define	STEP_ID					"step"

//Element Data
#define	POINT_ID				"p"
#define	LINE_ID					"l"
#define	FACE_ID					"f"
#define	CURVE_ID				"curv"
#define	CURVE2D_ID				"curv2"
#define	SURFACE_ID				"surf"

//Free-form body statements
#define PARAMETERVAL_ID			"parm"
#define	OUTERTRIM_LOOP_ID		"trim"
#define	INNERTRIM_LOOP_ID		"hole"
#define SPECIALCURVE_ID			"scrv"
#define SPECIALPOINT_ID			"sp"
#define ENDSTATEMENT_ID			"end"

//Connectivity between free-forms
#define CONNECTION_ID			"con"

//Grouping
#define GROUPNAME_ID			"g"
#define	SMOOTHGROUP_ID			"s"
#define	MERGEGROUP_ID			"mg"
#define OBJECTNAME_ID			"o"

//Display & Render attributes
#define BEVERLINTERP_ID			"bevel"
#define	COLORINTERP_ID			"c_interp"
#define DISSOLVEINTERP_ID		"d_interp"
#define	LOD_ID					"lod"
#define MATERIAL_ID				"usemtl"
#define	MATERIALLIB_ID			"mtllib"
#define SHADOWCAST_ID			"shadow_obj"
#define RAYTRACE_ID				"trace_obj"
#define	CURVEAPPROX_ID			"ctech"
#define SURFACEAPPROX_ID		"stech"

//Material Library Identifiers
#define	MTL_LIBRARY_REF			"newmtl"
#define	MTL_LIBRARY_AMBIENTTEXT	"map_Ka"
#define MTL_LIBRARY_SPECTEXT    "map_Ks"
#define MTL_LIBRARY_DIFFUSETEXT "map_Kd"
#define MTL_LIBRARY_BUMPTEXT    "map_Bump"
#define MTL_LIBRARY_OPACITYTEXT "map_d"
#define	MTL_LIBRARY_AMBIENT		"Ka"
#define MTL_LIBRARY_DIFFUSE		"Kd"
#define MTL_LIBRARY_SPECULAR	"Ks"
#define MTL_LIBRARY_SHININESS	"Ns"

#endif