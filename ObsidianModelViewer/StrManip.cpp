/*	Author: Frank Dye
	Date: 9/19/13
	Copyright: Frank Dye, 9/19/2013
	Description: Implementation of StrManip header, misc string manipulation functions
*/

#include "StrManip.h"

int	AStrToI	(char* str)
{
	//Function to convert Ascii null terminated String to an integer. 
	//It returns this integer.
	
	int theNum= 0;
	int index = 0;

	while (str[index] != '\0')
	{
		theNum += int(str[index] - 48);
		if (str[index+1] != '\0')
			theNum *= 10;
		
		index++;
	}

	return theNum;
}

float	AStrToF	(char* str)
{
	//Function that converts a null terminated string to a float
	float theNum = 0;
	bool hitPeriod = false;
	float temp = 0;
	int precisionCount = 0;
	int index = 0;

	while (str[index] != '\0')
	{
		if (str[index] == '.')
			hitPeriod = true;
		else
		{
			if (!hitPeriod)
			{
				theNum += float(str[index] - 48);
				if (str[index+1] != '\0' && str[index+1] != '.')
					theNum *=10;
			}
			else
			{
				precisionCount++;
				temp =float(str[index] - 48) / float(pow((float) 10, (float)precisionCount));
				theNum+=temp;
			}
		}
		index++;
	}

	return theNum;
}

double	AStrToD	(char* str)
{
	//Function that converts a null terminated string to a double
	double theNum = 0;
	bool hitPeriod = false;
	double temp = 0;
	int precisionCount = 0;
	int index = 0;

	while (str[index] != '\0')
	{
		if (str[index] == '.')
			hitPeriod = true;
		else
		{
			if (!hitPeriod)
			{
				theNum += double(str[index] - 48);
				if (str[index+1] != '\0' && str[index+1] != '.')
					theNum *=10;
			}
			else
			{
				precisionCount++;
				temp =double(str[index] - 48) / ( pow(float(10), precisionCount));
				theNum+=temp;
			}
		}
		index++;
	}

	return theNum;
}
