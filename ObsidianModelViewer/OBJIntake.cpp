/*	Author: Frank Dye
	Date: 9/19/13
	Copyright: Frank Dye, 9/19/2013
	Description: Defines the entry point for the application.
*/

#include <fstream>
#include <iostream>
#include <windows.h>
#include <cstdlib>
#include <cstring>
#include <math.h>
#include <d3d11.h>
#include <d3dx11.h>
#include "Resource.h"



#include "COBJIntake.h"
#include "CCamera.h"
#include "CModel.h"
#include "CObject.h"
#include "CMaterialManager.h"
#include "Elements.h"


//Global pointers for D3D


RenderTextureRange*		pTextureRanges;
CCamera*                pSceneCam;  
CMaterialManager*		pMatManager;
CObject*				pObject;
CModel*					pModel;

//NEW CODE FOR D3D11

//--------------------------------------------------------------------------------------
// Global Variables
//--------------------------------------------------------------------------------------
HINSTANCE               g_hInst = NULL;
HWND                    g_hWnd = NULL;
D3D_DRIVER_TYPE         g_driverType = D3D_DRIVER_TYPE_NULL;
D3D_FEATURE_LEVEL       g_featureLevel = D3D_FEATURE_LEVEL_11_0;
ID3D11Device*           g_pd3dDevice = NULL;
ID3D11DeviceContext*    g_pImmediateContext = NULL;
IDXGISwapChain*         g_pSwapChain = NULL;
ID3D11RenderTargetView* g_pRenderTargetView = NULL;


//--------------------------------------------------------------------------------------
// Forward declarations
//--------------------------------------------------------------------------------------
HRESULT InitWindow( HINSTANCE hInstance, int nCmdShow );
HRESULT InitDevice();
void CleanupDevice();
LRESULT CALLBACK    WndProc( HWND, UINT, WPARAM, LPARAM );
void Render();




//--------------------------------------------------------------------------------------
// Entry point to the program. Initializes everything and goes into a message processing 
// loop. Idle time is used to render the scene.
//--------------------------------------------------------------------------------------
int WINAPI wWinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow )
{
    UNREFERENCED_PARAMETER( hPrevInstance );
    UNREFERENCED_PARAMETER( lpCmdLine );

    if( FAILED( InitWindow( hInstance, nCmdShow ) ) )
        return 0;

    if( FAILED( InitDevice() ) )
    {
        CleanupDevice();
        return 0;
    }

    // Main message loop
    MSG msg = {0};
    while( WM_QUIT != msg.message )
    {
        if( PeekMessage( &msg, NULL, 0, 0, PM_REMOVE ) )
        {
            TranslateMessage( &msg );
            DispatchMessage( &msg );
        }
        else
        {
            Render();
        }
    }

    CleanupDevice();

    return ( int )msg.wParam;
}


//--------------------------------------------------------------------------------------
// Register class and create window
//--------------------------------------------------------------------------------------
HRESULT InitWindow( HINSTANCE hInstance, int nCmdShow )
{
    // Register class
    WNDCLASSEX wcex;
    wcex.cbSize = sizeof( WNDCLASSEX );
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = WndProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = LoadIcon( hInstance, ( LPCTSTR )IDI_TUTORIAL1 );
    wcex.hCursor = LoadCursor( NULL, IDC_ARROW );
    wcex.hbrBackground = ( HBRUSH )( COLOR_WINDOW + 1 );
    wcex.lpszMenuName = NULL;
    wcex.lpszClassName = L"TutorialWindowClass";
    wcex.hIconSm = LoadIcon( wcex.hInstance, ( LPCTSTR )IDI_TUTORIAL1 );
    if( !RegisterClassEx( &wcex ) )
        return E_FAIL;

    // Create window
    g_hInst = hInstance;
    RECT rc = { 0, 0, 640, 480 };
    AdjustWindowRect( &rc, WS_OVERLAPPEDWINDOW, FALSE );
    g_hWnd = CreateWindow( L"TutorialWindowClass", L"Direct3D 11 Tutorial 1: Direct3D 11 Basics", WS_OVERLAPPEDWINDOW,
                           CW_USEDEFAULT, CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top, NULL, NULL, hInstance,
                           NULL );
    if( !g_hWnd )
        return E_FAIL;

    ShowWindow( g_hWnd, nCmdShow );

    return S_OK;
}


//--------------------------------------------------------------------------------------
// Called every time the application receives a message
//--------------------------------------------------------------------------------------
LRESULT CALLBACK WndProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
    PAINTSTRUCT ps;
    HDC hdc;

    switch( message )
    {
        case WM_PAINT:
            hdc = BeginPaint( hWnd, &ps );
            EndPaint( hWnd, &ps );
            break;

        case WM_DESTROY:
            PostQuitMessage( 0 );
            break;

        default:
            return DefWindowProc( hWnd, message, wParam, lParam );
    }

    return 0;
}


//--------------------------------------------------------------------------------------
// Create Direct3D device and swap chain
//--------------------------------------------------------------------------------------
HRESULT InitDevice()
{
    HRESULT hr = S_OK;

    RECT rc;
    GetClientRect( g_hWnd, &rc );
    UINT width = rc.right - rc.left;
    UINT height = rc.bottom - rc.top;

    UINT createDeviceFlags = 0;
#ifdef _DEBUG
    createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

    D3D_DRIVER_TYPE driverTypes[] =
    {
        D3D_DRIVER_TYPE_HARDWARE,
        D3D_DRIVER_TYPE_WARP,
        D3D_DRIVER_TYPE_REFERENCE,
    };
    UINT numDriverTypes = ARRAYSIZE( driverTypes );

    D3D_FEATURE_LEVEL featureLevels[] =
    {
        D3D_FEATURE_LEVEL_11_0,
        D3D_FEATURE_LEVEL_10_1,
        D3D_FEATURE_LEVEL_10_0,
    };
	UINT numFeatureLevels = ARRAYSIZE( featureLevels );

    DXGI_SWAP_CHAIN_DESC sd;
    ZeroMemory( &sd, sizeof( sd ) );
    sd.BufferCount = 1;
    sd.BufferDesc.Width = width;
    sd.BufferDesc.Height = height;
    sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    sd.BufferDesc.RefreshRate.Numerator = 60;
    sd.BufferDesc.RefreshRate.Denominator = 1;
    sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    sd.OutputWindow = g_hWnd;
    sd.SampleDesc.Count = 1;
    sd.SampleDesc.Quality = 0;
    sd.Windowed = TRUE;

    for( UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++ )
    {
        g_driverType = driverTypes[driverTypeIndex];
        hr = D3D11CreateDeviceAndSwapChain( NULL, g_driverType, NULL, createDeviceFlags, featureLevels, numFeatureLevels,
                                            D3D11_SDK_VERSION, &sd, &g_pSwapChain, &g_pd3dDevice, &g_featureLevel, &g_pImmediateContext );
        if( SUCCEEDED( hr ) )
            break;
    }
    if( FAILED( hr ) )
        return hr;

    // Create a render target view
    ID3D11Texture2D* pBackBuffer = NULL;
    hr = g_pSwapChain->GetBuffer( 0, __uuidof( ID3D11Texture2D ), ( LPVOID* )&pBackBuffer );
    if( FAILED( hr ) )
        return hr;

    hr = g_pd3dDevice->CreateRenderTargetView( pBackBuffer, NULL, &g_pRenderTargetView );
    pBackBuffer->Release();
    if( FAILED( hr ) )
        return hr;

    g_pImmediateContext->OMSetRenderTargets( 1, &g_pRenderTargetView, NULL );

    // Setup the viewport
    D3D11_VIEWPORT vp;
    vp.Width = (FLOAT)width;
    vp.Height = (FLOAT)height;
    vp.MinDepth = 0.0f;
    vp.MaxDepth = 1.0f;
    vp.TopLeftX = 0;
    vp.TopLeftY = 0;
    g_pImmediateContext->RSSetViewports( 1, &vp );

    return S_OK;
}


//--------------------------------------------------------------------------------------
// Render the frame
//--------------------------------------------------------------------------------------
void Render()
{
    // Just clear the backbuffer
    float ClearColor[4] = { 0.0f, 0.125f, 0.3f, 1.0f }; //red,green,blue,alpha
    g_pImmediateContext->ClearRenderTargetView( g_pRenderTargetView, ClearColor );
    g_pSwapChain->Present( 0, 0 );
}


//--------------------------------------------------------------------------------------
// Clean up the objects we've created
//--------------------------------------------------------------------------------------
void CleanupDevice()
{
    if( g_pImmediateContext ) g_pImmediateContext->ClearState();

    if( g_pRenderTargetView ) g_pRenderTargetView->Release();
    if( g_pSwapChain ) g_pSwapChain->Release();
    if( g_pImmediateContext ) g_pImmediateContext->Release();
    if( g_pd3dDevice ) g_pd3dDevice->Release();
}



//OLD CODE



#define D3DVertexDescriptor (D3DFVF_XYZ|D3DFVF_NORMAL|D3DFVF_TEX1|D3DFVF_TEXCOORDSIZE2(0))

int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{

    COBJIntake	Obj;
	
    char        szTempFileName[1024];
	errno_t     test;
	FILE*		hFile = NULL;

	//Make the drawing window.
	WNDCLASSEX WindowClass = {sizeof(WNDCLASSEX), CS_CLASSDC, WndProc, 0L, 0L,
							  GetModuleHandle(NULL), NULL, NULL, NULL, NULL,
							  "OBJIntake", NULL};
	
	RegisterClassEx(&WindowClass);

	HWND hWnd = CreateWindow("OBJIntake", "OBJIntake Create Device", WS_OVERLAPPEDWINDOW,
							 100, 100, 300, 300, GetDesktopWindow(), NULL, WindowClass.hInstance,
							 NULL);

	if (SUCCEEDED(InitD3D(hWnd)))
	{
		pMatManager = new CMaterialManager;
		pMatManager->SetD3DDevice(pD3D9Dev);

		//Open the File
     	_getcwd(szTempFileName, 1024);
		strcat_s(szTempFileName, "\\Obj\\");
		strcat_s(szTempFileName, "aug.obj");

		test = fopen_s(&hFile, szTempFileName, "r");
		Obj.SetMaterialManager(pMatManager);
		Obj.LoadFile(hFile);

		_fcloseall();
		
		//Load the object.
		int modelNumVertices;
		int modelNumMeshes;
		
		modelNumVertices = Obj.GetNumD3DVertices();
		modelNumMeshes = Obj.GetNumMeshes();
		
		D3DVector* vectorList;
		vectorList = Obj.GetD3DVectorArray();

		Mesh* meshList;
		meshList = Obj.GetMeshArray();

		pModel = new CModel(vectorList, modelNumVertices, meshList, modelNumMeshes);
		pObject = new CObject(pModel);

        //Initialize are camera!
       	D3DXVECTOR3*	camPos = new D3DXVECTOR3(0.0f, 40.0f, -120.0f);
    	D3DXVECTOR3*	camTarget = new D3DXVECTOR3(0.0f, 40.0f, 0.0f);

        pSceneCam = new CCamera(pD3D9Dev, *camPos, *camTarget,  1.0f, 1000.0f);

        //Delete those vectors now they are copied
        delete  camPos;
        delete  camTarget;

		CreateScene();
		ShowWindow(hWnd, SW_SHOWDEFAULT);
		UpdateWindow(hWnd);

		//Process internal messages

		bool end = false;
		while (!end) {
			MSG msg;
			while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
			{
				if (!GetMessage(&msg, NULL, 0, 0)) {
					end = true;
					break;
				}

				TranslateMessage(&msg);
				DispatchMessage(&msg);
				UpdateWindow(hWnd);

			}
            RenderScene();
		}

		//Destroy D3D
		if( pD3D9Dev != NULL) 
			pD3D9Dev->Release();

		if( pD3D9 != NULL)
			pD3D9->Release();

	}
	UnregisterClass("OBJIntake", WindowClass.hInstance);

	
	return 0;
}



HRESULT 	InitD3D(HWND hWnd)
{
	//Generic code to initialize D3D

	//Create D3D object
	if (NULL == (pD3D9 = Direct3DCreate9(D3D_SDK_VERSION)))
		return E_FAIL;

	D3DDISPLAYMODE D3DDispMode;

	//Request default disp mode
	if (FAILED(pD3D9->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &D3DDispMode)))
		return E_FAIL;

	//Define the device paramters
	D3DPRESENT_PARAMETERS D3DParameters;
	ZeroMemory(&D3DParameters, sizeof(D3DParameters));
	D3DParameters.Windowed = true;
	D3DParameters.SwapEffect = D3DSWAPEFFECT_DISCARD;
    D3DParameters.EnableAutoDepthStencil = true;
    D3DParameters.AutoDepthStencilFormat = D3DFMT_D16;
	D3DParameters.BackBufferFormat = D3DDispMode.Format;

	//Create the device for drawing
	if (FAILED(pD3D9->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
								  D3DCREATE_SOFTWARE_VERTEXPROCESSING, 
								  &D3DParameters, &pD3D9Dev)))
	{
		return	E_FAIL;
	}

	return S_OK;
}

LRESULT WINAPI WndProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam )
{	
	switch(msg)
	{
		case WM_DESTROY: 
			PostQuitMessage(0);
			return 0;

		case WM_PAINT:
			RenderScene();
			ValidateRect(hWnd, NULL);
			return 0;
        
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);
}

void	UpdateTransform()
{
    D3DXMATRIX  translationMat;
    D3DXMATRIX  rotationMat;
    D3DXVECTOR3 camTarget;
    D3DXVECTOR3 camPos;
    D3DXVECTOR3 negTranslation;
    D3DXVECTOR3 posTranslation;
    D3DXVECTOR3 finalCamPos;
    D3DXVECTOR3 tempVec3;

    float finalCamDist;

    camTarget = pSceneCam->GetCameraTarget();
    camPos = pSceneCam->GetCameraPosition();

    negTranslation = camTarget * -1;
    posTranslation = camTarget;

    D3DXMatrixTranslation(&translationMat, negTranslation.x, negTranslation.y, negTranslation.z);
    
    D3DXVec3TransformCoord(&finalCamPos, &camPos, &translationMat);

    D3DXMatrixRotationYawPitchRoll(&rotationMat, D3DX_PI / 50, 0.0f, 0.0f);

    D3DXVec3TransformCoord(&finalCamPos, &finalCamPos, &rotationMat);

    D3DXMatrixTranslation(&translationMat, posTranslation.x, posTranslation.y, posTranslation.z);

    D3DXVec3TransformCoord(&finalCamPos, &finalCamPos, &translationMat);
    
    tempVec3 = finalCamPos - camTarget;
    finalCamDist = D3DXVec3Length(&tempVec3);

    pSceneCam->SetCameraPosition(finalCamPos);

    pSceneCam->UpdateView();     

}


void	CreateScene()
{
	int numVectors;

	D3DVector*	pVBVectors;

	D3DVector* pVectorList;

	numVectors = pModel->GetNumVertices();

	pVectorList = pModel->GetWorldSpaceVertices();

	//Open a vertexBuffer with enough room to hold all are faces
	pD3D9Dev->CreateVertexBuffer(sizeof(D3DVector) * numVectors,
								0, D3DVertexDescriptor, D3DPOOL_DEFAULT, &pVB, NULL);

	//Lock the  buffer at the beginning
	pVB->Lock(0, sizeof(D3DVector) * numVectors, (VOID**) &pVBVectors, 0);

	memcpy(pVBVectors, pVectorList, sizeof(D3DVector) * numVectors);

	//Release the VB which holds a whole mess of vertices
	pVB->Unlock();

}

void	RenderScene()
{
	int	numMeshes, materialID;

	Mesh*	pMeshList;
	const MaterialDesc*	pActiveMaterial;

	numMeshes = pModel->GetNumMeshes();

	pMeshList = pModel->GetMeshList();

	int numTriangles, i;

    //Set the camera transform
	UpdateTransform();

	if (NULL != pD3D9)
	{	
		//Write all of the buffer to blue
		pD3D9Dev->Clear(0, NULL, D3DCLEAR_TARGET&&D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 0, 255), 1.0f, 0);
		pD3D9Dev->Clear(0, NULL, D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 0, 255), 1.0f, 0);
		pD3D9Dev->Clear(0, NULL, D3DCLEAR_STENCIL, D3DCOLOR_XRGB(0, 0, 255), 1.0f, 0);

		//Begin scene
		pD3D9Dev->BeginScene();


		//Stream to hardware
		pD3D9Dev->SetStreamSource( 0, pVB, 0,sizeof(D3DVector) );


        //Setup default ambient white light
        D3DCOLOR  ambientColor;
        ambientColor = D3DCOLOR_RGBA(255, 255, 255, 255);
        pD3D9Dev->SetRenderState(D3DRS_AMBIENT, ambientColor);
		pD3D9Dev->SetRenderState(D3DRS_LIGHTING , FALSE);
        pD3D9Dev->SetRenderState(D3DRS_ZENABLE, TRUE);


		//Set Fixed Vertex Format
		pD3D9Dev->SetFVF(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX2);
		
		for (i = 0; i < numMeshes; i++)
		{
			materialID = pMeshList[i].materialID;

			pActiveMaterial = pMatManager->GetMaterialHandle(materialID);

		    //Setup the texture & material
			if (pActiveMaterial->pAmbientTextureFileName != NULL)
			{
			    pD3D9Dev->SetTexture( 0, pActiveMaterial->pAmbientTexture);
	            pD3D9Dev->SetTextureStageState(0, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_COUNT2);
		        pD3D9Dev->SetTextureStageState(0, D3DTSS_TEXCOORDINDEX, 0);         
				pD3D9Dev->SetTextureStageState( 0, D3DTSS_COLOROP,   D3DTOP_MODULATE );
				pD3D9Dev->SetTextureStageState( 0, D3DTSS_COLORARG1, D3DTA_TEXTURE );
				pD3D9Dev->SetTextureStageState( 0, D3DTSS_COLORARG2, D3DTA_DIFFUSE );
				pD3D9Dev->SetTextureStageState( 0, D3DTSS_ALPHAOP,   D3DTOP_DISABLE );
			}
			
			if (pActiveMaterial->pDiffuseTextureFileName != NULL)
			{
			    pD3D9Dev->SetTexture( 0, pActiveMaterial->pDiffuseTexture);
	            pD3D9Dev->SetTextureStageState(0, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_COUNT2);
		        pD3D9Dev->SetTextureStageState(0, D3DTSS_TEXCOORDINDEX, 0);         
				pD3D9Dev->SetTextureStageState( 0, D3DTSS_COLOROP,   D3DTOP_MODULATE );
				pD3D9Dev->SetTextureStageState( 0, D3DTSS_COLORARG1, D3DTA_TEXTURE );
				pD3D9Dev->SetTextureStageState( 0, D3DTSS_COLORARG2, D3DTA_DIFFUSE );
				pD3D9Dev->SetTextureStageState( 0, D3DTSS_ALPHAOP,   D3DTOP_DISABLE );
			}

            pD3D9Dev->SetMaterial(&pActiveMaterial->mat);

            //Calculate the vertice index and num triangles to render

			numTriangles = (((pMeshList[i].stopIndex + 1 - pMeshList[i].startIndex)) / 3);

            //Draw triangles
            pD3D9Dev->DrawPrimitive( D3DPT_TRIANGLELIST, pMeshList[i].startIndex, numTriangles);
		}
	

		//Release the VertexBuffer
		pVB->Release();

		//End Scene
		pD3D9Dev->EndScene();

		//Present backbuff for viewing
		pD3D9Dev->Present(NULL, NULL, NULL, NULL);



	}
}