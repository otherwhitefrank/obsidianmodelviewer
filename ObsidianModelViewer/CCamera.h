/*	Author: Frank Dye
	Date: 9/19/13
	Copyright: Frank Dye, 9/19/2013
	Description: A very simple camera class for the purposes of demonstration
*/


#ifndef	CCAMERA_HEADER
#define CCAMERA_HEADER

#include <windows.h>
#include <d3d11.h>
#include <d3dx11.h>

class   CCamera
{
public:
    
    CCamera(LPDIRECT3DDEVICE9 inDevice);
    CCamera(LPDIRECT3DDEVICE9 inDevice, D3DXVECTOR3 camPos, D3DXVECTOR3 camTarget, float nearClip, float farClip);
    CCamera(LPDIRECT3DDEVICE9 inDevice, D3DXMATRIX viewMat, D3DXMATRIX projMat); //setup the viewpoint matrix

    ~CCamera();

    //Set camera parameter functions
    void SetCameraPosition(D3DXVECTOR3);
    void SetCameraTarget(D3DXVECTOR3);
    void SetCameraViewMatrix(D3DXMATRIX);
    void SetCameraProjectionMatrix(D3DXMATRIX);
    void SetCameraRoll(float);
    void SetCameraPitch(float);
    void SetCameraYaw(float);


    //Request camera info functions
    D3DXVECTOR3 GetCameraPosition();
    D3DXVECTOR3 GetCameraTarget();

    float   GetCameraRoll();
    float   GetCameraPitch();
    float   GetCameraYaw();

    D3DXMATRIX  GetViewMatrix();
    D3DXMATRIX  GetProjectionMatrix();

    //Update viewport
    void    UpdateView();
    void    ResetCamera();

private:
    float   camRoll;
    float   camPitch;
    float   camYaw;

    D3DXVECTOR3 camPos;
    D3DXVECTOR3 camTarget;
    D3DXVECTOR3 upVect;

    D3DXMATRIX  viewMat;
    D3DXMATRIX  projMat;
    D3DXMATRIX  worldMat;

    float   nearClipPlane;
    float   farClipPlane;
    
    //Pointer to the device
    LPDIRECT3DDEVICE9   pD3D9LocalDev;
};
#endif