/*	Author: Frank Dye
	Date: 9/19/13
	Copyright: Frank Dye, 9/19/2013
	Description: Misc functions for manipulation of strings.
*/

#ifndef	STRMANIP_HEADER
#define STRMANIP_HEADER

#include <math.h>

//Prototypes for String conversion functions
int		AStrToI	(char* str);
float	AStrToF	(char* str);
double	AStrToD	(char* str);

#endif